import 'package:bookshelfapp/screens/stories_list_screen.dart';
import 'package:flutter/material.dart';
import 'package:audioplayers/audio_cache.dart';



void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Bookshelf',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: StoriesList(title: 'Bookshelf'),
    );
  }
}


