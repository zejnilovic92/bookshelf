import 'dart:async';
import 'dart:convert';

import 'package:audioplayers/audio_cache.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:bookshelfapp/model/benchmark_data.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/services.dart';

AudioCache cache = AudioCache();
AudioPlayer player;


class PlayStory extends StatefulWidget {
  PlayStory({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _PlayStoryState createState() => _PlayStoryState();
}

class _PlayStoryState extends State<PlayStory> {

  List <IconData> playPauseIcons = [Icons.play_circle_filled, Icons.pause_circle_filled];
  bool playing = true;

  List benchmarks = [];
  int index = 0;

  Future<String> loadBenchmarksJson () async {
    return await rootBundle.loadString("assets/json_benchmarks/thepoweroffamily.json");
  }
  Future loadBenchmarks() async{
    String jsonString = await loadBenchmarksJson();
    final jsonresponse  = json.decode(jsonString);
    BenchmarkData benchmarkData = new BenchmarkData.fromJson(jsonresponse);
    benchmarks = benchmarkData.benchmarks;
    print(benchmarkData.benchmarks[0]);
  }

  CarouselSlider carouselSlider;
  CarouselController buttonCarouselController = CarouselController();

  String getAudioPosition(Duration duration) {
    String twoDigits(int n) {
      if (n >= 10) return "$n";
      return "0$n";
    }
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "$twoDigitMinutes:$twoDigitSeconds";
  }

  void playSound() async{
    player = await cache.play('Von-Trapps-The-Power-of-Family.mp3');
    player.onAudioPositionChanged.listen((Duration  p) {
      if(getAudioPosition(p) == benchmarks[index]){
        buttonCarouselController.nextPage();
        index++;
      }
      print("current " + getAudioPosition(p));
      print("current " + benchmarks[index]);
    });
  }
  @override
  void initState() {
    loadBenchmarks();
    Timer(Duration(seconds: 2), () {
      playSound();
    });
    super.initState();
  }

  @override
  void dispose() {
    player.stop();
    super.dispose();
  }

  int _current = 0;
  List imgList = [
  //  "assets/images/CCF_0001.jpg",
  //  "assets/images/CCF_0002.jpg",
    "assets/images/CCF_0003.jpg",
    "assets/images/CCF_0004.jpg",
    "assets/images/CCF_0005.jpg",
    "assets/images/CCF_0006.jpg",
    "assets/images/CCF_0007.jpg",
    "assets/images/CCF_0008.jpg",
    "assets/images/CCF_0009.jpg",
    "assets/images/CCF_0010.jpg",
    "assets/images/CCF_0011.jpg",
    "assets/images/CCF_0012.jpg",
    "assets/images/CCF_0013.jpg",
    "assets/images/CCF_0014.jpg",
    "assets/images/CCF_0015.jpg",
    "assets/images/CCF_0016.jpg",
    "assets/images/CCF_0017.jpg",
    "assets/images/CCF_0018.jpg",
    "assets/images/CCF_0019.jpg",
    "assets/images/CCF_0020.jpg",
    "assets/images/CCF_0021.jpg",
    "assets/images/CCF_0022.jpg",
    "assets/images/CCF_0023.jpg",
    "assets/images/CCF_0024.jpg",
    "assets/images/CCF_0025.jpg",
    "assets/images/CCF_0026.jpg",
    "assets/images/CCF_0027.jpg",
    "assets/images/CCF_0028.jpg",
    "assets/images/CCF_0029.jpg",
    "assets/images/CCF_0030.jpg",
    "assets/images/CCF_0031.jpg",
    "assets/images/CCF_0032.jpg",
    "assets/images/CCF_0033.jpg",
    "assets/images/CCF_0034.jpg",
    "assets/images/CCF_0035.jpg",
    "assets/images/CCF_0036.jpg",
    "assets/images/CCF_0037.jpg",
    "assets/images/CCF_0038.jpg",
    "assets/images/CCF_0039.jpg",
    "assets/images/CCF_0040.jpg",
    "assets/images/CCF_0041.jpg",
    "assets/images/CCF_0042.jpg",
    "assets/images/CCF_0043.jpg",
    "assets/images/CCF_0044.jpg",
    "assets/images/CCF_0045.jpg",
    "assets/images/CCF_0046.jpg",
    "assets/images/CCF_0047.jpg",
    "assets/images/CCF_0048.jpg",
    "assets/images/CCF_0049.jpg",
    "assets/images/CCF_0050.jpg",
    "assets/images/CCF_0051.jpg",
    "assets/images/CCF_0052.jpg",
    "assets/images/CCF_0053.jpg",
    "assets/images/CCF_0054.jpg",
    "assets/images/CCF_0055.jpg",
    "assets/images/CCF_0056.jpg",
    "assets/images/CCF_0057.jpg",
    "assets/images/CCF_0058.jpg",
    "assets/images/CCF_0059.jpg",
    "assets/images/CCF_0060.jpg",
  ];

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }
    return result;
  }


  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        body: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                flex: 9,
                child: carouselSlider = CarouselSlider(
                  carouselController: buttonCarouselController,
                  options: CarouselOptions(
                    height: double.infinity,
                    initialPage: 0,
                    enlargeCenterPage: true,
                    autoPlay: false,
                    reverse: false,
                    enableInfiniteScroll: true,
                    autoPlayInterval: Duration(seconds: 2),
                    autoPlayAnimationDuration: Duration(milliseconds: 2000),
                    scrollDirection: Axis.horizontal,
                    onPageChanged: (index, reason) {
                      setState(() {
                        _current = index;
                      });
                    },
                  ),
                  items: imgList.map((imgUrl) {
                    return Builder(
                      builder: (BuildContext context) {
                        return Container(
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.symmetric(horizontal: 10.0),
                          decoration: BoxDecoration(
                            color: Colors.green,
                          ),
                          child: Image.asset(
                            imgUrl,
                            fit: BoxFit.fill,
                          ),
                        );
                      },
                    );
                  }).toList(),
                ),
              ),
              SizedBox(
                height: 10,
              ),
              Expanded(
                flex: 1,
                child: Container(
                  height: 90,
                  width: 90,
                  child: IconButton(
                    iconSize: 50.0,
                    icon: playing ? Icon(playPauseIcons[1]) : Icon(playPauseIcons[0]),onPressed: (){
                    setState(() {
                      if(playing)
                        player.pause();
                      else
                        player.resume();
                      playing = !playing;
                    });
                  },),
                )
              ),
            ],
          ),
        ),
      ),
    );
  }

  goToPrevious() {
    buttonCarouselController.previousPage(
        duration: Duration(milliseconds: 300), curve: Curves.ease);
  }

  goToNext() {
    buttonCarouselController.nextPage(
        duration: Duration(milliseconds: 300), curve: Curves.decelerate);
  }

  Future<bool> _onBackPressed() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Go back'),
        content: new Text('Are you sure you want to leave?'),
        actions: <Widget>[
          new GestureDetector(
            onTap: () => Navigator.of(context).pop(false),
            child: Text("CANCEL"),
          ),
          SizedBox(height: 16),
          new GestureDetector(
            onTap: () => Navigator.of(context).pop(true),
            child: Text("YES"),
          ),
        ],
      ),
    ) ??
        false;
  }

  foo() async {
    await showDialog(
      context: context,
      builder: (context) => AlertDialog(
        title: new Text("Go back"),
        content: new Text(
            "Are you sure you want to leave?"),
        actions: <Widget>[
          new FlatButton(
            child: new Text("Yes"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          new FlatButton(
            child: new Text("Cancel"),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
        ],
      ),
    );
    Navigator.pop(context);
  }
}