import 'package:audioplayers/audio_cache.dart';
import 'package:bookshelfapp/model/story.dart';
import 'package:bookshelfapp/screens/play_story_screen.dart';
import 'package:flutter/material.dart';

class StoriesList extends StatefulWidget {
  StoriesList({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _StoriesListState createState() => _StoriesListState();
}

class _StoriesListState extends State<StoriesList> {

  List<Story> stories = [
    Story("assets/images/CCF.jpg",'Von-Trapps-The-Power-of-Family.mp3'),
    Story("assets/images/CCF.jpg",'Von-Trapps-The-Power-of-Family.mp3'),
    Story("assets/images/CCF.jpg",'Von-Trapps-The-Power-of-Family.mp3'),
    Story("assets/images/CCF.jpg",'Von-Trapps-The-Power-of-Family.mp3'),
    Story("assets/images/CCF.jpg",'Von-Trapps-The-Power-of-Family.mp3'),
    Story("assets/images/CCF.jpg",'Von-Trapps-The-Power-of-Family.mp3'),
    Story("assets/images/CCF.jpg",'Von-Trapps-The-Power-of-Family.mp3'),
    Story("assets/images/CCF.jpg",'Von-Trapps-The-Power-of-Family.mp3'),
    Story("assets/images/CCF.jpg",'Von-Trapps-The-Power-of-Family.mp3'),
    Story("assets/images/CCF.jpg",'Von-Trapps-The-Power-of-Family.mp3'),
    Story("assets/images/CCF.jpg",'Von-Trapps-The-Power-of-Family.mp3'),
    Story("assets/images/CCF.jpg",'Von-Trapps-The-Power-of-Family.mp3'),
    Story("assets/images/CCF.jpg",'Von-Trapps-The-Power-of-Family.mp3'),
    Story("assets/images/CCF.jpg",'Von-Trapps-The-Power-of-Family.mp3'),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: ListView.builder(
          itemCount: stories.length,
          itemBuilder: (context, index){
            return Card(
              child: ListTile(
                onTap: () {
                  Navigator.push(context, new MaterialPageRoute(builder: (context)=>PlayStory(),),);
                },
                title: Text("The power of family"),
                subtitle: Text("Featuring the story of the Trapp family"),
                leading: Image.asset("assets/images/CCF.jpg"),
              ),
            );
      },
      ),
    );
  }
}