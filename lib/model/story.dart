class Story {
  String storyThumbnailPath;
  String audioPath;

  Story(this.storyThumbnailPath, this.audioPath);
}