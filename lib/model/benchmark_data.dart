class BenchmarkData{
  final List benchmarks;

  BenchmarkData({this.benchmarks});

  factory BenchmarkData.fromJson(Map<String, dynamic> json){
    return BenchmarkData(
        benchmarks: parseBenchmarks(json['benchmarks'])
    );
  }

  static List<String> parseBenchmarks (benchmarksJson){
    List<String> benchmarksList = new List<String>.from(benchmarksJson);
    return benchmarksList;
  }
}